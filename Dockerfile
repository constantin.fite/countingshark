FROM nvcr.io/nvidia/l4t-pytorch:r32.5.0-pth1.7-py3

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y
RUN python3 -m pip install --upgrade setuptools
RUN python3 -m pip install --upgrade pip
RUN apt-get install libopencv-dev -y
RUN python3 -m pip install requests seaborn pandas pycocotools thop
RUN apt-get install python-pip protobuf-compiler libprotoc-dev -y
RUN python3 -m pip install cmake 
RUN python3 -m pip install onnx 
RUN python3 -m pip install Cython --user

RUN apt install -y cmake libgtk2.0-dev wget


RUN python3 -m pip install scikit-build
RUN python3 -m pip uninstall numpy -y
RUN python3 -m pip install numpy==1.19.4 
RUN python3 -m pip install opencv-python
RUN python3 -m pip install tqdm PyYAML scipy


WORKDIR /countingshark
