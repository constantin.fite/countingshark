
import sys
import os

if not os.path.exists('/home/xavier/Documents/countingshark/yolov5-3.1/count-shark/'):
	os.makedirs('/home/xavier/Documents/countingshark/yolov5-3.1/count-shark/')

file1 = open('/home/xavier/Documents/countingshark/GPS/file.txt', 'r')
Lines = file1.readlines()
 
count = 0
lati=0
longi=0


def lat(t):
  return (int(t[0:2]) + (float(t[2:9])/60))


def lng(g): 
  return (int(g[0:3]) + (float(g[3:10])/60))

for line in Lines:
    count += 1
    if("$GPRMC" in line):
	print("find GPRMC")
        lati = lat(line[20:29])
        longi = lng(line[32:42])
        break
    else:
	print("don't find") 
	

f = open("/home/xavier/Documents/countingshark/yolov5-3.1/count-shark/"+sys.argv[1]+".txt", "a")
f.write("lat= "+str(lati)+" long= "+str(longi)+"\n")
print("Get GPS OK")
f.close()
