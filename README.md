<div align="center">
<p>Ce repository contient tout le code développé pendant le stage de dernière année de Constantin FITE au sein du LIRMM dans l'équipe ICAR de <b>février à juillet 2021</b></p>
</div>

# Primary commands

## Record and run the detection next

`./pipeline_gps_record_detect.sh`

## Record the video stream for the 4 camera separatly

The video are saved in 4 folder in this location :
_/home/xavier/Documents/countingshark/record-rtsp/records/camera_IP_adress/.mp4_

`./pipeline_record_individual.sh`

## Record the video stream for the 4 camera concatenated

The video are saved in 4 folder in this location :

_/home/xavier/Documents/countingshark/record-rtsp/records/mosaic/_

`./pipeline_record_mosaic.sh`

## Change settings camera

This script allows to change the FPS, resolution and settings of the camera. Normally FPS and resolution doesn't need to be changed. 
They have to stay at **5 FPS** and **1280x720px**

`cd change-settings-camera && python3 interface-graphic.py`


# Communication with iridium

`python3 send_data_iridium.py`

## To decompress csv file access via rockblock :

`python3 decompression.py`


# Others informations

- **compare** folder contains all the test made to find the optimal treatment chain
- **gps** folder contains 


## **Login:Password**
- For the Jetson Xavier : **jetson-xavier:BoueeDCP**
- For the IP camera: **admin:admin**

Adress IP : 
- 192.168.1.18
- 192.168.1.19
- 192.168.1.20
- 192.168.1.21

## See the power consumption of the Jetson

`jtop`

JetPack 4.5.1 installé

# YOLOv5

## Installation de l'environnement de YOLOv5  :

- Créer l'image docker à partir du dockerfile

`sudo docker build -t yolov5-docker .` (Dockerfile)

(https://ngc.nvidia.com/catalog/containers/nvidia:l4t-pytorch) doc

- Lancer l'image yolov5-docker

`sudo ./run_docker_YOLO.sh`

## YOLOv5 version :

Version yolov5 pour lancer la détection: commit _86f4247_ 3.1
https://github.com/ultralytics/yolov5/tree/86f4247515dfb4cccd413c236ff26b31d8e066b4

version pour test.py (matrice de confusion et résultats : d4456e43 4.0)

https://github.com/ultralytics/yolov5/tree/d4456e43b23be03dfd5098d2a1992cd338581801
 
## Lancer la détection 

`cd yolov5-3.1`

`python3 detect.py --source ./streams.txt --weights ../best-shark-yolov5s.pt --conf 0.7 --name 1280_5FPS  --classes 1`


# Clavier fr us
setxkbmap fr
setxkbmap us

# Others

<details>
<summary>Way to improve performance
</summary>


## TensorRT

git clone -b yolov5-v3.1 https://github.com/wang-xinyu/tensorrtx.git


## Run with Deepstream


https://github.com/marcoslucianops/DeepStream-Yolo

Run detection

cd /opt/nvidia/deepstream/deepstream-5.1/sources/yolo

`deepstream-app -c deepstream_app_config.txt`

- enable fps

`export NVDS_ENABLE_LATENCY_MEASUREMENT=1`

</details>
<br>

# Acknowledgment

## <p>Tuteur de stage </p>

- Frédérick Comby
- Marc Chaumont
- Gérard Subsol
- Pierrick Serres

Thanks also to Fabien Forget for his help

# Contact

Email : constantin.fite@lirmm.fr / constantin.fite@gmail.com


