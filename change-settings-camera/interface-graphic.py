import tkinter as tk
from tkinter import *
import requests
import xml.etree.ElementTree as ET


xml = ""
Setting = ["Hue", 'Brightness', "Saturation",
           "Sharpness", "Contrast",  "Denoise", ]
parent = Tk()
parent.geometry("1000x700")
SliderList = []
optionMenuList = []
stringVarList = []
count = 0
count_name = 0


# DROP DOWN MENU
OPTIONS_RESOLUTION = [
    "1920x1080",
    "1280x960",
    "1280x720",
    "720x576",
    "720x480",
    "640x480",

]

OPTIONS_IPS = [
    "3",
    "5",
    "10",
    "15",
    "20",
    "25",
    "30"
]

text=["Resolution","FPS"]
drop_down_option = [OPTIONS_RESOLUTION, OPTIONS_IPS]


# GET IPS RESOLUTION

Url = "http://admin:admin@192.168.2.21/cgi/major_stream_get?Channel=1&Group=StreamInfo"
r = requests.get(url=Url)
root = ET.fromstring(r.content)
arrayData = []

for node in root.iter():
    count += 1

    if count == 9 or count == 15:
        arrayData.append(node.text)


for i in range(2):
    labelTest = tk.Label(text=text[i], font=('Helvetica', 12), fg='black')
    labelTest.pack(side="top")
    stringVarList.append(StringVar(parent))
    stringVarList[i].set(arrayData[i])  # default value
    optionMenuList.append(OptionMenu(parent, stringVarList[i], *drop_down_option[i]))
    optionMenuList[i].pack()
    
# GET SATURATION PARAMETER

Url = "http://admin:admin@192.168.2.21/cgi/image_get?Channel=1&Group=ImageInfo"
r = requests.get(url=Url)
root = ET.fromstring(r.content)
arrayData2 = []
count = 0
for node in root.iter():
    count += 1
    if count % 2 == 0 and count > 7:
        arrayData2.append(node.text)

for j in range(6):
    SliderList.append(Scale(parent, label=Setting[j], from_=0, to=255,
                            orient=HORIZONTAL, length=200))

    SliderList[j].set(int(arrayData2[j]))
    SliderList[j].pack()

# POST SATURATION

def postSaturation():
    for i in range(4):
        cameraIndex = str(i+18)
        Url = "http://admin:admin@192.168.2."+cameraIndex+"/cgi/image_set?\
Channel=1&Group=ImageInfo&Hue="+str(SliderList[0].get())+"&Brightness="+str(SliderList[1].get())+"&Sharpness\
="+str(SliderList[2].get())+"&Saturation="+str(SliderList[3].get())+"&Contrast="+str(SliderList[4].get())+"&Denoise="+str(SliderList[5].get())+"&Gamma=4"
        requests.post(url=Url)


def postIPS():
    for i in range(4):
        cameraIndex = str(i+18)

        Url = "http://admin:admin@192.168.2."+cameraIndex+\
            "/cgi/major_stream_set?Channel=1&Group=StreamInfo&FrameRate="+stringVarList[1].get()+"&Resolution="+stringVarList[0].get()

        requests.post(url=Url)


def post():
    postSaturation()
    postIPS()


Button(parent, text='Set value', command=post).pack()

mainloop()
