#!/bin/bash

echo "Test connexion caméras"

ADR1=192.168.2.18
ADR2=192.168.2.19
ADR3=192.168.2.20
ADR4=192.168.2.21

stop=0
ok=0
i=0
while (( $stop == 0 ))
do
    echo "Test caméra 1 "
    ping -c 1 $ADR1
    RET1=$?

    echo "Test caméra 2 "
    ping -c 1 $ADR2
    RET2=$?

    echo "Test caméra 3 "
    ping -c 1 $ADR3
    RET3=$?

    echo "Test caméra 4 "
    ping -c 1 $ADR4
    RET4=$?

#    echo " Reponse Cam 1 : $RET1"
#    echo " Reponse Cam 2 : $RET2"
#    echo " Reponse Cam 3 : $RET3"
#    echo " Reponse Cam 4 : $RET4"

    if [ $RET1 == 0 ]
    then echo "Camera 1 OK"
    else echo "Camera 1 NON OK"
    fi

    if [ $RET2 == 0 ]
    then echo "Camera 2 OK"
    else echo "Camera 2 NON OK"
    fi

    if [ $RET3 == 0 ]
    then echo "Camera 3 OK"
    else echo "Camera 3 NON OK"
    fi

    if [ $RET4 == 0 ]
    then echo "Camera 4 OK"
    else echo "Camera 4 NON OK"
    fi

    if [ $RET1 == 0 ] && [ $RET2 == 0 ] && [ $RET3 == 0 ] && [ $RET4 == 0 ]
    then
        echo "4 Cameras OK"
		stop=1
        ok=1
    else
        echo "Manque une ou plusieurs camera(s)"
        
		((i=i+1)) # Incrementation nombre de tentatives
		echo "$i tentatives sur 3"
		if [ $i == 3 ]
		then
			echo "Fin des tentatives. STOP."
			stop=1
		else
			echo "Nouvelle tentative dans 3s."
			sleep 1
			echo "."
			sleep 1
			echo "."
			sleep 1
		fi
    fi

done
