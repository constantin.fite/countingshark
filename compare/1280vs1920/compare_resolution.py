import cv2
import numpy as np
import os
import matplotlib.image as img
import pandas as pd
import random
import matplotlib.pyplot as plt
import statistics

real_source = '/home/jetson-nano/Documents/shark/countingshark/video/soyeux1/'
txt_source = '/home/jetson-nano/Documents/shark/countingshark/video/soyeux1/extracted_frame_5FPS/count_shark.txt'


output = "/home/jetson-nano/Documents/shark/countingshark/compare/1280vs1920/"

file_shark_1280 = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/1280_5FPS2021-4-2 18:6:12.txt"
file_shark_1920 = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/1920_5FPS2021-4-2 18:8:57.txt"

file_shark_1280_soyeux2 = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/1280_5FPS_soyeux22021-4-2 16:45:30.txt"
file_shark_1920_soyeux2 = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/1920_5FPS_soyeux22021-4-2 16:30:31.txt"


file_number = 0
c_real = []
c_predicted = []
c_diff=[]
#number_real_file = sum(f.endswith('.txt') for f in os.listdir(real_source))
number_real_file = sum(1 for line in open(txt_source))

#number_predicted_file = sum(f.endswith('.txt')
#                           for f in os.listdir(predicted_source))


if not os.path.exists(output):
    os.makedirs(output)

conf_threshold = 0.7

# count number of shark for groundtruth label


with open(txt_source,'r') as f:
    
    for i, x in enumerate (f):

        c_real.append(int(x))


# Compare 1920/1280

with open(file_shark_1280,'r') as f:
    count=0
    c_predicted_1280=[]
    for i, line in enumerate (f):
        x = line.split()
        conf_sum=0
        count_shark = int(x[0]) 
        if count_shark >0:
            for j in range (count_shark):

                conf_sum += float(x[j+1])
            c_predicted_1280.append(conf_sum)
        else:
            c_predicted_1280.append(0)

with open(file_shark_1920,'r') as f:
    count=0
    c_predicted_1920=[]
    
    for i, line in enumerate (f):
        x = line.split()
        conf_sum=0
        count_shark = int(x[0])
        if count_shark >0:
            for j in range (count_shark):

                conf_sum += float(x[j+1])
            c_predicted_1920.append(conf_sum)
        else:
            c_predicted_1920.append(0)


number_frame_over = abs(len(c_predicted_1280)-len(c_predicted_1920))

c_predicted_1280 = c_predicted_1280[:len(c_predicted_1280)-number_frame_over]
"""
number_frame_over = abs(len(c_predicted_1280)-len(c_real))

c_diff_1280 = np.subtract(np.array(c_real), np.array(c_predicted_1280))
c_diff_1920 = np.subtract(np.array(c_real), np.array(c_predicted_1920))

sum_1280 = np.sum(c_diff_1280)
sum_1920 = np.sum(c_diff_1920)
print(sum_1280)
print(sum_1920)
"""

c_diff = np.array(c_predicted_1920) - np.array(c_predicted_1280)

print('number above 0.5',(c_diff >0.5).sum())
above= (c_diff >0.5).sum()
bellow = (c_diff <-0.5).sum()
print('number bellow 0.5',(c_diff <-0.5).sum())

print('percentage above 0.5',(c_diff >0.5).sum()/len(c_diff))
print('percentage bellow 0.5',(c_diff <-0.5).sum()/len(c_diff))
# save to graph x: image y: number shark (real and detected)

fig = plt.figure(figsize=(12,8),dpi=80)

#ax = plt.axes()

x_real=np.arange(len(c_real))

x_1280 = np.arange(len(c_predicted_1280))
x_1920 = np.arange(len(c_predicted_1920))


#axs[0].scatter(x_1280, c_predicted_1280, label='resolution 1280*720 ',s=10,color=[1.,0.5,0.5])
#axs[0].scatter(x_1920, c_predicted_1920, label='resolution 1920*1080',s=10,color=[0.5,0.5,1.])

#axs[0].scatter(x_real, c_real, label='true label',s=30,color=[0.8,0.1,0.9])
plt.scatter(x_1920, c_diff, label='resolution 1920x1080 - resolution 1280x720',s=10,color=[0.5,0.5,1.])
#axs[1].scatter(x_real, c_real, label='true label',s=30,color=[0.8,0.1,0.9])
#axs[0].legend(prop={'size':20})
plt.legend(prop={'size':13})
fig.text(0.7,0.01,'> 0.5='+str(above),fontsize=20)
fig.text(0.7,0.05,'< -0.5 ='+str(bellow),fontsize=20)
fig.text(0.15,0.05,'Total number of frame ='+str(len(x_1920)),fontsize=15)
#axs[0].set_title('Real number of shark and number of shark detected with process record at 5 FPS and then detection (1280*720) and (1920*1080)')
plt.title('Difference number of shark detected with process record at 5 FPS and then detection between 1920 and 1280 resolution')
fig.text(0.5,0.04,'Frame number',ha='center')
fig.text(0.07,0.5,'Sum of confidence scores',va="center",rotation='vertical')

# plt.show()
plt.savefig(output+"compare_number_shark.png")

# 

# Draw on image
"""
for i in range(number_real_file):

    img = cv2.imread(os.path.join(real_source, str(i+1)+".jpeg"))
    if os.path.isfile(os.path.join(predicted_source, str(i+1)+".txt")):
        f_predicted = open(os.path.join(predicted_source, str(i+1)+".txt"),
                           "r")

    f_real = open(os.path.join(real_source, str(i+1)+".txt"),
                  "r")

    for line in f_predicted:
        line = line.split()
        if float(line[5]) >conf_threshold:
            x = float(line[1])
            y = float(line[2])
            w = float(line[3])
            h = float(line[4])

            # convert x,y,w,h to x1,y1,x2,y2

            H, W, _ = img.shape
            xyxy = [(x - w / 2) * W, (y - h / 2) * H,
                    (x + w / 2) * W, (y + h / 2) * H]
            label = "predicted"
            plot_one_box(xyxy, img, label=label,
                        color=[0, 0, 255], line_thickness=3)


    for x in f_real:
        word_list = x.split()
        #xyxy = [0,0,0,0]
        #xyxy[0] =  word_list[0]
        
        label = "real"
        plot_one_box(word_list, img, label=label,
                     color=[255, 0, 0], line_thickness=2)
        
        cv2.imwrite(real_source+"\\compare_dir\\"+str(i)+".jpg", img)

    f.close()
"""