import cv2
import numpy as np
import os
import matplotlib.image as img
import pandas as pd
import random
import matplotlib.pyplot as plt
import statistics
import matplotlib.ticker as ticker
from matplotlib.transforms import Bbox


process_continu_time_jetson = [0, 30, 30, 45, 45, 120, 120, 180]
process_continu_time_camera = [0, 30, 30, 45, 45, 120, 120, 180]
process_continu_power_jetson = np.array([2.5, 2.5, 3.5, 3.5, 3.5, 3.5, 7, 7])
process_continu_power_camera = 4*np.array(
    [1.08, 1.08, 1.08, 1.08, 2.3, 2.3, 2.3, 2.3])
process_continu_sum = process_continu_power_camera + process_continu_power_jetson

process_continu_joule = 2384
time_tot_continu = 180

record_without_concat_time_jetson = [
    0, 30, 30, 45, 45, 90, 90, 105, 105, 155, 155, 255, 255, 355, 355, 455, 455, 555, 555]
record_without_concat_time_camera = [
    0, 30, 30, 45, 45, 90, 90, 105, 105, 155, 155, 255, 255, 355, 355, 455, 455, 555, 555]
record_without_concat_power_jetson = np.array(
    [2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 3.5, 3.5, 5, 5, 5, 5, 5, 5, 5, 5, 5])
record_without_concat_power_camera = 4*np.array(
    [1.08, 1.08, 1.08, 1.08, 2.3, 2.3, 2.3, 2.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
record_without_concat_sum = record_without_concat_power_camera + \
    record_without_concat_power_jetson

process_withoutconcat_joule = 3671
time_tot_without = 555

record_with_concat_time_jetson = [
    0, 30, 30, 45, 45, 90, 90, 105, 105, 185, 185, 235, 235, 475]
record_with_concat_time_camera = [
    0, 30, 30, 45, 45, 90, 90, 105, 105, 185, 185, 235, 235, 475]
record_with_concat_power_jetson = np.array(
    [2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 4.3, 4.3, 3.5, 3.5, 5.5, 5.5])
record_with_concat_power_camera = 4*np.array(
    [1.08, 1.08, 1.08, 1.08, 2.3, 2.3, 2.3, 2.3, 0, 0, 0, 0, 0, 0])
record_with_concat_sum = record_with_concat_power_camera + \
    record_with_concat_power_jetson

record_concat_time_jetson = [
    0, 30, 30, 45, 45, 105, 105, 155, 155, 395]
record_concat_time_camera = [
    0, 30, 30, 45, 45, 105, 105, 155, 155, 395]
record_concat_power_jetson = np.array(
    [2.5, 2.5, 2.5, 2.5, 4, 4, 3.5, 3.5, 5.2, 5.2])
record_concat_power_camera = 4*np.array(
    [1.08, 1.08, 1.08, 1.08, 2.3, 2.3, 0, 0, 0, 0])
record_concat_sum = record_concat_power_camera + \
    record_concat_power_jetson

process_withconcat_joule = 2810
time_tot_with = 475

fig, ax = plt.subplots(nrows=4, sharex=False, figsize=(25, 30), dpi=200)
fig.tight_layout(pad=7)
# Continu detection
# ax[0].set_title(
#   'Process "continuous detection" over 1 minute (caméras résolution 1280*720 5FPS)', fontsize=20)
ax[0].plot(process_continu_time_jetson, process_continu_power_jetson,
           label='jetson power', color=[0.5, 0.5, 1.])
ax[0].plot(process_continu_time_camera, process_continu_power_camera,
           label='camera power', color=[1, 0.5, 1.])
ax[0].plot(process_continu_time_camera, process_continu_sum,
           label='total power', color=[1, 0.5, 0.5])
ax[0].set_xlabel('Time (second)', fontsize=20)
ax[0].set_ylabel('Power (Watt)', fontsize=20)
ax[0].tick_params(axis='both', which='major', labelsize=20)

#ax[0].yaxis.set_ticks(np.arange(0, 10, 1))
ax[0].text(0, 2.5, 'jetson starting', fontsize=20)
ax[0].text(0, 4.32, 'cameras starting', fontsize=20)
ax[0].text(30, 3.5, 'network starting', fontsize=20)
ax[0].text(120, 7, 'beginning detection', fontsize=20)
ax[0].legend(prop={'size': 20})

# Record without concat
# ax[1].set_title(
#    'Process " record over 1 minute then detection without concatenation " (caméras résolution 1280*720 5FPS)')
ax[1].plot(record_without_concat_time_jetson, record_without_concat_power_jetson,
           label='puissance jetson', color=[0.5, 0.5, 1.])
ax[1].plot(record_without_concat_time_camera, record_without_concat_power_camera,
           label='puissance camera', color=[1, 0.5, 1.])
ax[1].plot(record_without_concat_time_camera, record_without_concat_sum,
           label='puissance total', color=[1, 0.5, 0.5])
ax[1].set_xlabel('Time (second)', fontsize=20)
ax[1].set_ylabel('Power (Watt)', fontsize=20)
ax[1].text(155, 5, 'beginning detection', fontsize=20)
ax[1].tick_params(axis='both', which='major', labelsize=20)
ax[1].text(0, 2.5, 'jetson starting', fontsize=20)
ax[1].text(0, 4.32, 'cameras starting', fontsize=20)
ax[1].text(45, 9.4, 'cameras recording', fontsize=20)


# Record with concat
# ax[2].set_title(
#    'Process record over 1 minute then detection with concatenation(caméras résolution 1280*720 5FPS)')
ax[2].plot(record_with_concat_time_jetson, record_with_concat_power_jetson,
           label='puissance jetson', color=[0.5, 0.5, 1.])
ax[2].plot(record_with_concat_time_camera, record_with_concat_power_camera,
           label='puissance camera', color=[1, 0.5, 1.])
ax[2].plot(record_with_concat_time_camera, record_with_concat_sum,
           label='puissance total', color=[1, 0.5, 0.5])
ax[2].set_xlabel('Time (second)', fontsize=20)
ax[2].set_ylabel('Power (Watt)', fontsize=20)
ax[2].text(45, 2.5, 'Record start', fontsize=20)
ax[2].text(105, 4.3, 'concatenation', fontsize=20)
ax[2].text(185, 3.5, 'Network starting réseau', fontsize=20)
ax[2].text(235, 5.5, 'detection', fontsize=20)
ax[2].tick_params(axis='both', which='major', labelsize=20)
ax[2].text(0, 4.32, 'cameras starting', fontsize=20)
ax[2].text(45, 9.4, 'cameras recording', fontsize=20)


# Record concat
# ax[3].set_title(
#    'Process record with concatenation then detection (resolution camera 1280*720 5FPS)')
ax[3].plot(record_concat_time_jetson, record_concat_power_jetson,
           label='power jetson', color=[0.5, 0.5, 1.])
ax[3].plot(record_concat_time_camera, record_concat_power_camera,
           label='power camera', color=[1, 0.5, 1.])
ax[3].plot(record_concat_time_camera, record_concat_sum,
           label='power total', color=[1, 0.5, 0.5])

ax[3].set_xlabel('Time (seconde)', fontsize=20)
ax[3].set_ylabel('Power (Watt)', fontsize=20)

#ax[2].yaxis.set_ticks(np.arange(0, 9, 1))
ax[3].text(0, 2.5, 'jetson starts', fontsize=20)
ax[3].text(0, 4.4, 'cameras starting', fontsize=20)
ax[3].tick_params(axis='both', which='major', labelsize=20)
ax[3].text(105, 3.5, 'Network starting réseau', fontsize=20)
ax[3].text(155, 5.2, 'detection', fontsize=20)
ax[3].text(45, 9.4, 'cameras recording', fontsize=20)


def full_extent(ax, pad=0.0):
    """Get the full extent of an axes, including axes labels, tick labels, and
    titles."""
    # For text objects, we need to draw the figure first, otherwise the extents
    # are undefined.
    ax.figure.canvas.draw()
    items = ax.get_xticklabels() + ax.get_yticklabels()
#    items += [ax, ax.title, ax.xaxis.label, ax.yaxis.label]
    items += [ax, ax.title]
    bbox = Bbox.union([item.get_window_extent() for item in items])

    return bbox.expanded(1.1 + pad, 1.1 + pad)
# plt.show()


fig.savefig('./full_figure.png')
for i in range(4):
    extent = full_extent(ax[i]).transformed(fig.dpi_scale_trans.inverted())
    fig.savefig('./ax'+str(i)+'_figure.png', bbox_inches=extent)
