import cv2
import numpy as np
import os
import matplotlib.image as img
import pandas as pd
import random
import matplotlib.pyplot as plt
import statistics

txt_source = '/home/jetson-nano/Documents/shark/countingshark/video/soyeux2/extracted_frame_5FPS/count.txt'

predicted_image = '/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/runs/detect/soyeux_image/'
txt_predicted = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/soyeux22021-4-30 15:8:35.txt"

output = "/home/jetson-nano/Documents/shark/countingshark/compare/compare_soyeux2/"

file_number = 0
c_real = []
c_real_long = []
c_predicted = []
c_diff=[]
#number_real_file = sum(f.endswith('.txt') for f in os.listdir(real_source))
number_real_file = sum(1 for line in open(txt_source))

#number_predicted_file = sum(f.endswith('.txt')
#                           for f in os.listdir(predicted_source))


if not os.path.exists(output):
    os.makedirs(output)

conf_threshold = 0.7

# count number of shark for groundtruth label


with open(txt_source,'r') as f:
    
    for i, x in enumerate (f):
        c_real.append(int(x))
        for i in range(5):
            c_real_long.append(int(x))
             

# Compare FPS

with open(txt_predicted,'r') as f:
    count=0
    c_5FPS=[]
    for i, line in enumerate (f):
        x = line.split()
        conf_sum=0
        count_shark = int(x[0]) 
        if count_shark >0:
            for j in range (count_shark):

                conf_sum += float(x[j+1])
            c_5FPS.append(conf_sum)
        else:
            c_5FPS.append(0)




# save to graph x: image y: number shark (real and detected)
fig = plt.figure(figsize=(12,8),dpi=80)

#ax = plt.axes()

x_real=np.arange(len(c_real))
x_real_long=np.arange(len(c_real_long))

plt.scatter(x_real, c_real, label='True label',s=10,color=[1,0.5,1.])
plt.scatter(x_real, c_5FPS, label='Number of sharks detected by yolo',s=10,color=[0.5,0.5,1.])

fig.text(0.5,0.04,'Frame number',ha='center')
fig.text(0.07,0.5,'Sum of confidence scores',va="center",rotation='vertical')

plt.legend(prop={'size':15})

plt.savefig(output+"compare_soyeux.png")

# 

# Draw on image
"""
for i in range(number_real_file):

    img = cv2.imread(os.path.join(real_source, str(i+1)+".jpeg"))
    if os.path.isfile(os.path.join(predicted_source, str(i+1)+".txt")):
        f_predicted = open(os.path.join(predicted_source, str(i+1)+".txt"),
                           "r")

    f_real = open(os.path.join(real_source, str(i+1)+".txt"),
                  "r")

    for line in f_predicted:
        line = line.split()
        if float(line[5]) >conf_threshold:
            x = float(line[1])
            y = float(line[2])
            w = float(line[3])
            h = float(line[4])

            # convert x,y,w,h to x1,y1,x2,y2

            H, W, _ = img.shape
            xyxy = [(x - w / 2) * W, (y - h / 2) * H,
                    (x + w / 2) * W, (y + h / 2) * H]
            label = "predicted"
            plot_one_box(xyxy, img, label=label,
                        color=[0, 0, 255], line_thickness=3)


    for x in f_real:
        word_list = x.split()
        #xyxy = [0,0,0,0]
        #xyxy[0] =  word_list[0]
        
        label = "real"
        plot_one_box(word_list, img, label=label,
                     color=[255, 0, 0], line_thickness=2)
        
        cv2.imwrite(real_source+"\\compare_dir\\"+str(i)+".jpg", img)

    f.close()
"""
