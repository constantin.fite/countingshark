import cv2
import numpy as np
import os

import pandas as pd
import random
import matplotlib.pyplot as plt
import statistics
import math

real_source = '/home/jetson-nano/Documents/shark/countingshark/video/soyeux1/'
txt_source = '/home/jetson-nano/Documents/shark/countingshark/video/soyeux1/extracted_frame_5FPS/count_shark.txt'


file_concatenated = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/concatenated_img_size12802021-4-2 14:45:58.txt"
file_1 = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/1st_part_concat2021-4-2 20:2:26.txt"
file_2 = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/2nd_part_concat2021-4-2 20:6:48.txt"
file_3 = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/3rd_part_concat2021-4-2 21:6:18.txt"
file_4 = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/4th_part_concat2021-4-2 20:12:46.txt"

output = "/home/jetson-nano/Documents/shark/countingshark/compare/concat_vs_notconcat/"

file_number = 0
c_real = []
c_real_long = []
c_predicted = []
c_diff=[]
#number_real_file = sum(f.endswith('.txt') for f in os.listdir(real_source))
number_real_file = sum(1 for line in open(txt_source))

#number_predicted_file = sum(f.endswith('.txt')
#                           for f in os.listdir(predicted_source))


if not os.path.exists(output):
    os.makedirs(output)

conf_threshold = 0.7

# count number of shark for groundtruth label


with open(txt_source,'r') as f:
    
    for i, x in enumerate (f):
        c_real.append(int(x))
        for i in range(5):
            c_real_long.append(int(x))
             

# Compare concatenation

with open(file_concatenated,'r') as f:
    count=0
    c_concat=[]
    for i, line in enumerate (f):
        x = line.split()
        conf_sum=0
        count_shark = int(x[0]) 
        if count_shark >0:
            for j in range (count_shark):

                conf_sum += float(x[j+1])
            c_concat.append(conf_sum)
        else:
            c_concat.append(0)

#first

with open(file_1,'r') as f:
    count=0
    c_1=[]
    
    for i, line in enumerate (f):
        
        x = line.split()
        conf_sum=0
        count_shark = int(x[0])
        if count_shark >0:
            
            for j in range (count_shark):
                
                conf_sum += float(x[j+1])
            c_1.append(conf_sum)
        else:
            
            c_1.append(0)

#second

with open(file_2,'r') as f:
    count=0
    c_2=[]
    
    for i, line in enumerate (f):
        
        x = line.split()
        conf_sum=0
        count_shark = int(x[0])
        if count_shark >0:
            
            for j in range (count_shark):
                
                conf_sum += float(x[j+1])
            c_2.append(conf_sum)
        else:
            
            c_2.append(0)

#3rd 

with open(file_3,'r') as f:
    count=0
    c_3=[]
    
    for i, line in enumerate (f):
        
        x = line.split()
        conf_sum=0
        count_shark = int(x[0])
        if count_shark >0:
            
            for j in range (count_shark):
                
                conf_sum += float(x[j+1])
            c_3.append(conf_sum)
        else:
            
            c_3.append(0)

#4th

with open(file_4,'r') as f:
    count=0
    c_4=[]
    
    for i, line in enumerate (f):
        
        x = line.split()
        conf_sum=0
        count_shark = int(x[0])
        if count_shark >0:
            
            for j in range (count_shark):
                
                conf_sum += float(x[j+1])
            c_4.append(conf_sum)
        else:
            
            c_4.append(0)


min_length = min(len(c_1), len(c_2), len(c_4))

c_1 = c_1[:min_length]
c_2 = c_2[:min_length]
c_3 = c_3[:min_length]
c_4 = c_4[:min_length]


c_not_concat = np.array(c_1)+np.array(c_2)+np.array(c_3)+np.array(c_4)


"""
number_frame_over = abs(len(c_concat)-len(c_real))
c_real = c_real[:len(c_real)-number_frame_over]



c_diff_1280 = np.subtract(np.array(c_real), np.array(c_concat))
c_diff_1920 = np.subtract(np.array(c_real), np.array(c_1))

sum_1280 = np.sum(c_diff_1280)
sum_1920 = np.sum(c_diff_1920)
print(sum_1280)
print(sum_1920)
"""
c_diff = np.array(c_not_concat) - np.array(c_concat)

# save to graph x: image y: number shark (real and detected)

fig = plt.figure(figsize=(12,8),dpi=80)
#ax = plt.axes()

x_real=np.arange(len(c_real))
x_notconcat=  range(len(c_not_concat))
x_concat = range(len(c_concat))
plt.axhline(y=0,color='r', linestyle='-')

#plt.scatter(x_concat, c_concat, label='concat',s=15,color=[1.,0.5,0.5])
#axs[0].scatter(x_real, c_real, label='true label',s=30,color=[0.8,0.1,0.9])
plt.scatter(x_notconcat, c_diff, label='video not_concat - video concat',s=12,color=[0.5,0.5,1.])
plt.title('Difference between number of shark detected with not concatenated process and concatenated process')
#axs[1].scatter(x_real, c_real, label='true label',s=30,color=[0.8,0.1,0.9])
fig.text(0.5,0.04,'Frame number',ha='center')
fig.text(0.07,0.5,'Sum of confidence scores',va="center",rotation='vertical')

plt.legend(prop={'size':15})
# plt.show()
plt.savefig(output+"compare_number_shark.png")

# 

# Draw on image
"""
for i in range(number_real_file):

    img = cv2.imread(os.path.join(real_source, str(i+1)+".jpeg"))
    if os.path.isfile(os.path.join(predicted_source, str(i+1)+".txt")):
        f_predicted = open(os.path.join(predicted_source, str(i+1)+".txt"),
                           "r")

    f_real = open(os.path.join(real_source, str(i+1)+".txt"),
                  "r")

    for line in f_predicted:
        line = line.split()
        if float(line[5]) >conf_threshold:
            x = float(line[1])
            y = float(line[2])
            w = float(line[3])
            h = float(line[4])

            # convert x,y,w,h to x1,y1,x2,y2

            H, W, _ = img.shape
            xyxy = [(x - w / 2) * W, (y - h / 2) * H,
                    (x + w / 2) * W, (y + h / 2) * H]
            label = "predicted"
            plot_one_box(xyxy, img, label=label,
                        color=[0, 0, 255], line_thickness=3)


    for x in f_real:
        word_list = x.split()
        #xyxy = [0,0,0,0]
        #xyxy[0] =  word_list[0]
        
        label = "real"
        plot_one_box(word_list, img, label=label,
                     color=[255, 0, 0], line_thickness=2)
        
        cv2.imwrite(real_source+"\\compare_dir\\"+str(i)+".jpg", img)

    f.close()
"""