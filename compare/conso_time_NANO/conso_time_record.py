import cv2
import numpy as np
import os
import matplotlib.image as img
import pandas as pd
import random
import matplotlib.pyplot as plt
import statistics
import matplotlib.ticker as ticker


time_jetson = [0]
conso_jetson = []
time_camera = [0]
conso_camera = []

total_time_one_period = 0
count_periode = 0

nb_periode = 2
# minute
duration_record_minute = 5
# minute
delay = 2
# minute
#duration = 60*12

duration_record = duration_record_minute * 60
duration_detection = round(duration_record*4, 2)


for i in range(nb_periode):
    if i == 0:

        time_camera.append(45)
        time_camera.append(time_camera[len(time_camera)-1])
        conso_jetson.append(2.5)
        conso_jetson.append(2.5)

    time_camera.append(time_camera[len(time_camera)-1]+duration_record)
    time_camera.append(time_camera[len(time_camera)-1])
    time_camera.append(time_camera[len(time_camera)-1]+50)
    time_camera.append(time_camera[len(time_camera)-1])
    time_camera.append(time_camera[len(time_camera)-1]+duration_detection)
    time_camera.append(time_camera[len(time_camera)-1])
    time_camera.append(time_camera[len(time_camera)-1]+45)
    time_camera.append(time_camera[len(time_camera)-1])

    conso_camera.append(1.08)
    conso_camera.append(1.08)
    conso_camera.append(2.5)
    conso_camera.append(2.5)
    conso_camera.append(0)
    conso_camera.append(0)
    conso_camera.append(0)
    conso_camera.append(0)

    conso_jetson.append(4)
    conso_jetson.append(4)
    conso_jetson.append(3.5)
    conso_jetson.append(3.5)
    conso_jetson.append(5.5)
    conso_jetson.append(5.5)
    conso_jetson.append(5.5)
    conso_jetson.append(5.5)

conso_jetson.append(4)


# time_camera.append(time_camera[len(time_camera)-1]+45)
conso_camera.append(1.08)
conso_camera.append(1.08)

conso_camera.append(2.5)


hour_camera = np.round(np.array(time_camera)/60, 2)
conso_camera = np.array(conso_camera)*4

sum_conso = np.array(conso_camera)+np.array(conso_jetson)

fig = plt.figure(figsize=(20, 15), dpi=80)

print(time_camera)
print(sum_conso)

"""
print(len(time_camera))
print(len(conso_camera))
print(len(conso_jetson))
"""
# Continu detection
# x_real=np.arange(len(c_real))
# x_real_long=np.arange(len(c_real_long))


plt.plot(hour_camera, conso_jetson, label='conso jetson',
         color=[0.5, 0.5, 1.], linewidth=2, linestyle='-')
plt.plot(hour_camera, conso_camera, label='conso camera',
         color=[1, 0, 1.], linewidth=2, linestyle='-')
plt.plot(hour_camera, sum_conso, label='conso somme',
         color=[0, 0, 0], linewidth=4)


plt.legend(prop={'size': 15})
plt.xlabel('minutes')
plt.ylabel('Consommation Watt')
plt.title("Consumption Jetson Nano record then detection")


# plt.savefig(output+"compare_soyeux.png")

# Compute consommation

Joule_total = 0


for i in range(len(sum_conso)):

    if i % 2 == 0 and i > 1:
        time = time_camera[i]-time_camera[i-2]
        Joule_total += round(sum_conso[i-1]*time, 2)


fig.text(0.05, 0.03, 'Total time record =' +
         str(round(duration_record_minute*nb_periode, 2))+"minutes", fontsize=20)
fig.text(0.3, 0.03, 'Percentage time record / total time =' +
         str(round(duration_record_minute*nb_periode/(time_camera[-1]/60), 3)), fontsize=20)

plt.savefig("compare_conso_10P.png")

fig.text(0.05, 0.08, 'Total Joule ='+str(round(Joule_total, 2)), fontsize=20)

plt.savefig("./"+str(duration_record_minute)+"minute/compare_conso_record_" +
            str(duration_record_minute)+"m_.png")
