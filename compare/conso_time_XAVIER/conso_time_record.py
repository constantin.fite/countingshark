import cv2
import numpy as np
import os
import matplotlib.image as img
import pandas as pd
import random
import matplotlib.pyplot as plt
import statistics
import matplotlib.ticker as ticker


time_jetson = [0]
conso_jetson = []
time_camera = [0]
conso_camera = []
total_time_one_period = 0
count_periode = 0

nb_periode = 100
# minute
duration_record_minute = 1
# minute
delay = 2
# minute
duration = 60*12

duration_record = duration_record_minute * 60
duration_detection = round(duration_record/1.333, 2)


for i in range(nb_periode):

    if (time_camera[-1]/60 < duration and time_camera[-1]/60+total_time_one_period/60 < duration):
        if i >= 1:

            time_camera.append(time_camera[len(time_camera)-1])
        count_periode += 1
        time_camera.append(time_camera[len(time_camera)-1]+45)
        time_camera.append(time_camera[len(time_camera)-1])
        time_camera.append(time_camera[len(time_camera)-1]+duration_record)
        time_camera.append(time_camera[len(time_camera)-1])
        time_camera.append(time_camera[len(time_camera)-1]+15)
        time_camera.append(time_camera[len(time_camera)-1])
        time_camera.append(time_camera[len(time_camera)-1]+duration_detection)
        time_camera.append(time_camera[len(time_camera)-1])
        time_camera.append(time_camera[len(time_camera)-1]+delay*60)

        conso_camera.append(1.08)
        conso_camera.append(1.08)
        conso_camera.append(2.5)
        conso_camera.append(2.5)
        conso_camera.append(0)
        conso_camera.append(0)
        conso_camera.append(0)
        conso_camera.append(0)
        conso_camera.append(0)
        conso_camera.append(0)

        conso_jetson.append(4)
        conso_jetson.append(4)
        conso_jetson.append(4.5)
        conso_jetson.append(4.5)
        conso_jetson.append(4.5)
        conso_jetson.append(4.5)
        conso_jetson.append(8)
        conso_jetson.append(8)
        conso_jetson.append(0)
        conso_jetson.append(0)
        if i == 0:
            total_time_one_period = time_camera[-1]


# time_camera.append(time_camera[len(time_camera)-1]+45)
print(time_camera[-1]/60)
if(time_camera[-1]/60 < 120):
    xlabel = "minutes"
    x_data = np.round(np.array(time_camera)/60, 2)
else:
    xlabel = "hour"
    x_data = np.round(np.array(time_camera)/60/60, 2)


conso_camera = np.array(conso_camera)*4

sum_conso = np.array(conso_camera)+np.array(conso_jetson)

fig = plt.figure(figsize=(20, 15), dpi=80)
"""
print(time_camera)
print(conso_jetson)
print(conso_camera)
"""
"""
print(len(time_camera))
print(len(conso_camera))
print(len(conso_jetson))
"""
# Continu detection
# x_real=np.arange(len(c_real))
# x_real_long=np.arange(len(c_real_long))


plt.plot(x_data, conso_jetson, label='conso jetson',
         color=[0.5, 0.5, 1.], linewidth=2, linestyle='-')
plt.plot(x_data, conso_camera, label='conso camera',
         color=[1, 0, 1.], linewidth=2, linestyle='-')
plt.plot(x_data, sum_conso, label='conso somme',
         color=[0, 0, 0], linewidth=4)


plt.legend(prop={'size': 15})
plt.xlabel(xlabel)
plt.ylabel('Consommation Watt')
plt.title("Consumption Xavier NX record during " +
          str(duration_record_minute)+" minutes then detection ")


# plt.savefig(output+"compare_soyeux.png")

# Compute consommation

Joule_total = 0


for i in range(len(sum_conso)):

    if i % 2 == 0 and i > 1:
        time = time_camera[i]-time_camera[i-2]
        Joule_total += round(sum_conso[i-1]*time, 2)


fig.text(0.05, 0.08, 'Total Joule ='+str(round(Joule_total, 2)), fontsize=20)
fig.text(0.05, 0.03, 'Total time record =' +
         str(round(duration_record*count_periode/60, 2))+"minutes", fontsize=20)
fig.text(0.3, 0.03, 'Percentage time record / total time =' +
         str(round(duration_record*count_periode/60/(time_camera[-1]/60), 3)), fontsize=20)

plt.savefig("./"+str(duration_record_minute)+"minute/compare_conso_record_" +
            str(duration_record_minute)+"m_"+str(duration)+"minute.png")
# plt.show()
