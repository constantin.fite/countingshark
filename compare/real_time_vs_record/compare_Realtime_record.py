import cv2
import numpy as np
import os
import matplotlib.image as img
import pandas as pd

import random
import matplotlib.pyplot as plt
import statistics

real_source = '/home/jetson-nano/Documents/shark/countingshark/video/soyeux1/'
txt_source = '/home/jetson-nano/Documents/shark/countingshark/video/soyeux1/extracted_frame_5FPS/count_shark.txt'


file_record_then_detection = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/1280_5FPS2021-4-2 18:6:12.txt"
file_real_time = "/home/jetson-nano/Documents/shark/countingshark/yolov5-3.1/count-shark/real_time2021-4-2 16:22:8.txt"

output = "/home/jetson-nano/Documents/shark/countingshark/compare/real_time_vs_record/"



file_number = 0
c_real = []
c_predicted = []
c_diff=[]
number_real_file = sum(1 for line in open(txt_source))


if not os.path.exists(output):
    os.makedirs(output)

conf_threshold = 0.7

# count number of shark for groundtruth label


with open(txt_source,'r') as f:
    
    for i, x in enumerate (f):

        c_real.append(int(x))
             

# Compare real time and record

with open(file_record_then_detection,'r') as f:
    count=0
    c_record_then_detection=[]
    for i, line in enumerate (f):
        x = line.split()
        conf_sum=0
        count_shark = int(x[0]) 
        if count_shark >0:
            for j in range (count_shark):

                conf_sum += float(x[j+1])
            c_record_then_detection.append(conf_sum)
        else:
            c_record_then_detection.append(0)

#Start from the 15th frames
#5FPS capture each frame
frame_start = 14

with open(file_real_time,'r') as f:
    count=0
    c_real_time=[]
    time_detection =[]
    
    for i, line in enumerate (f):
        if i==frame_start:
            x = line.split()
            start_time = float(x[1])
        if i > frame_start:
            x = line.split()
            conf_sum=0
            count_shark = int(x[0])
            if count_shark >0:
                time_detection.append(round(float(x[count_shark+1])-start_time,2))
                for j in range (count_shark):
                    
                    conf_sum += float(x[j+1])
                c_real_time.append(conf_sum)
            else:
                time_detection.append(round(float(x[1])-start_time,2))
                c_real_time.append(0)

#c_diff = np.array(c_real_time) - np.array(c_record_then_detection)

# save to graph x: image y: number shark (real and detected)

fig, axs = plt.subplots(2,figsize=(20,15),dpi=80)

#ax = plt.axes()

x_real=np.arange(len(c_real))
x_real_time = np.array(time_detection)*5
x_record_then_detection = range(len(c_record_then_detection))

axs[0].scatter(x_record_then_detection, c_record_then_detection, label='record then detection',s=10,color=[1.,0.5,0.5])
axs[0].scatter(x_real, c_real, label='true label',s=30,color=[0.8,0.1,0.9])
axs[1].scatter(x_real_time, c_real_time, label='countinuous detection',s=10,color=[0.5,0.5,1.])
axs[1].scatter(x_real, c_real, label='true label',s=30,color=[0.8,0.1,0.9])
axs[0].legend(prop={'size':20})
axs[1].legend(prop={'size':20})
axs[0].set_title('Real number of shark and number of shark detected with process "record at 5 FPS and then detection" (1 camera 1280*720)')
axs[1].set_title('Real number of shark and number of shark detected with process "countinuous detection" with camera at 5 FPS (1 camera 1280*720)')
fig.text(0.5,0.04,'Frame number',ha='center')
fig.text(0.07,0.5,'Sum of confidence scores',va="center",rotation='vertical')


# plt.show()
plt.savefig(output+"compare_number_shark.png")

# 

# Draw on image
"""
for i in range(number_real_file):

    img = cv2.imread(os.path.join(real_source, str(i+1)+".jpeg"))
    if os.path.isfile(os.path.join(predicted_source, str(i+1)+".txt")):
        f_predicted = open(os.path.join(predicted_source, str(i+1)+".txt"),
                           "r")

    f_real = open(os.path.join(real_source, str(i+1)+".txt"),
                  "r")

    for line in f_predicted:
        line = line.split()
        if float(line[5]) >conf_threshold:
            x = float(line[1])
            y = float(line[2])
            w = float(line[3])
            h = float(line[4])

            # convert x,y,w,h to x1,y1,x2,y2

            H, W, _ = img.shape
            xyxy = [(x - w / 2) * W, (y - h / 2) * H,
                    (x + w / 2) * W, (y + h / 2) * H]
            label = "predicted"
            plot_one_box(xyxy, img, label=label,
                        color=[0, 0, 255], line_thickness=3)


    for x in f_real:
        word_list = x.split()
        #xyxy = [0,0,0,0]
        #xyxy[0] =  word_list[0]
        
        label = "real"
        plot_one_box(word_list, img, label=label,
                     color=[255, 0, 0], line_thickness=2)
        
        cv2.imwrite(real_source+"\\compare_dir\\"+str(i)+".jpg", img)

    f.close()
"""