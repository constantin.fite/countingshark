import cv2 
import numpy as np

cap = cv2.VideoCapture('../compare/concat_vs_notconcat/1.mp4',0)
cap1 = cv2.VideoCapture('../compare/concat_vs_notconcat/2.mp4',0)
cap2 = cv2.VideoCapture('../compare/concat_vs_notconcat/3.mp4',0)
cap3 = cv2.VideoCapture('../compare/concat_vs_notconcat/4.mp4',0)
width, height = int(cap.get(3)), int(cap.get(4))
print('width, height:', width, height)

fps = cap.get(5)
print('FPS:' ,fps)

out = cv2.VideoWriter('../compare/concat_vs_notconcat/concatenated.mp4', cv2.VideoWriter_fourcc(*'mp4v'),fps,(width*2,height*2))

while(cap.isOpened()):
    ret,frame = cap.read()
    ret1, frame1 = cap1.read()
    ret2, frame2 = cap2.read()
    ret3, frame3= cap3.read()


    if ret == True and ret1 == True and ret2 == True and ret3 == True :
        
        both = np.concatenate((frame,frame1),axis=1)
        both2 = np.concatenate((frame2,frame3),axis=1)
        final = np.concatenate((both,both2),axis=0)
        out.write(final)
        cv2.imshow('Frame', final)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    else:
        break

print("finish")
cap.release()
out.release()

cv2.waitKey(0)
cv2.destroyAllWindows()
