import serial
import time
from ctypes import sizeof
from random import randrange
import random
import time
import os
import zlib
import math
import struct
import sys
import numpy as np
import pandas as pd
from datetime import datetime

date_list = []
latitude = []
longitude = []
list_number_of_shark = []
max_shark_list = []

previous_date = 0

data_iridium = pd.read_csv("messages-1626683189855.csv")


# reverse column
data_iridium["Payload"] = data_iridium["Payload"].values[::-1]
print(data_iridium["Payload"])

# Drop NA
data_iridium.dropna(subset=["Payload"], inplace=True)

for idx, i in enumerate(data_iridium["Payload"]):

    if int(i[:8], 16) > 1594297538:
        time = int(i[:8], 16)

        zobj = zlib.decompressobj()
        number_sharks = []

        if (previous_date != time):

            # DATE
            date = datetime.fromtimestamp(time)
            date_list.append(date)

            # GPS
            gps_string = i[8:28]
            
            gps_hex_format = bytearray.fromhex(gps_string)
            print(list(gps_hex_format[:4]))

            # Convert to byte to float
            lat = struct.unpack('f', gps_hex_format[:4])[0]
            long = struct.unpack('f', gps_hex_format[5:9])[0]
            lat_orientation = gps_hex_format[4:5].decode("ascii")
            long_orientation = gps_hex_format[9:10].decode("ascii")

            # convert to decimal degree
            lat_dec_str = str(lat)
            lat_dec = int(lat_dec_str[:2])+float(lat_dec_str[2:])/60

            long_dec_str = str(long)
            long_dec = int(long_dec_str[:2])+float(long_dec_str[2:])/60

            if(lat_orientation == "S"):
                lat_dec = - lat_dec
            if(long_orientation == "W"):
                long_dec = - long_dec

            latitude.append(lat_dec)
            longitude.append(long_dec)

            # SHARK_DATA
            number_of_shark_compress = i[28:]
            number_bytes_format = bytearray.fromhex(number_of_shark_compress)
            decompressed_byte_data = zobj.decompress(number_bytes_format)

            for b in decompressed_byte_data:
                number_sharks.append(b)
            #print(len(number_sharks))
            list_number_of_shark.append(number_sharks)

            # Max shark
            length_data_list = len(list_number_of_shark)

            shark_max = max(list_number_of_shark[length_data_list-1])
            max_shark_list.append(shark_max)

        else:

            # SHARK_DATA
            number_of_shark_compress = i[8:]
            number_bytes_format = bytearray.fromhex(number_of_shark_compress)
            decompressed_byte_data = zobj.decompress(number_bytes_format)

            length_data_list = len(list_number_of_shark)

            # Add sharks to existing list
            for b in decompressed_byte_data:
                list_number_of_shark[length_data_list-1].append(b)
            #print(len(list_number_of_shark[length_data_list-1]))

            # Max shark

            shark_max = max(list_number_of_shark[length_data_list-1])
            
            if(shark_max > max_shark_list[-1]):
                max_shark_list[-1] = shark_max

        previous_date = int(i[:8], 16)

df = pd.DataFrame({'Date': date_list, 'latitude': latitude,
                   'longitude': longitude, "Max shark": max_shark_list,
                   'number_sharks': list_number_of_shark})
df.to_csv("shark_format.csv", index=False)
