# sh -x foo.sh
# create some data
"""
some_int = 2112
some_float = 42.123456789
some_text = "hello world"
text_len = len(some_text)

# create binary data
data = struct.pack("i", some_int)
data += struct.pack("f", some_float)
data += struct.pack("i", len(some_text))
data += struct.pack("{}s".format(text_len), some_text.encode())
"""
import struct
import serial
import time
from ctypes import sizeof
from random import randrange
import random
import time
import os
import zlib
import sys
import numpy as np
from math import *
import statistics


counterSomme = 0
max_shark = 0
arrayFive = []
arrayCount = []
arrayMedian = []

# median of n frame
nb_frame = 3
# FILTER DATA

f = open("../yolov5-3.1/count-shark/soyeux.txt", "r+")
lines = f.readlines()
"""
for line in lines:
    count = line.split(" ")[0]

    number = int(count)
    arrayFive.append(number)
    arrayCount.append(number)

    if(len(arrayFive) >= nb_frame):
        median = statistics.median(arrayFive)
        arrayFive = []
        arrayMedian.append(median)

max_shark = np.max(arrayMedian)

print("max shark median :", max_shark)

print(arrayCount)
print(arrayMedian)

"""
# WRITE NUMBER OF SHARK

f = open("count.txt", "w+")
while counterSomme < 3000:

    x = random.uniform(0, 1)
    counter = randrange(5)
    if(x > 0.7 and x < 0.9):
        rand = randrange(10)
        for j in range(counter):
            f.write(str(rand)+"\n")

    elif(x > 0.90):
        rand = randrange(20)
        for j in range(counter):
            f.write(str((rand))+"\n")

    else:
        for j in range(counter):
            f.write("0"+"\n")
    counterSomme += counter

f.close()

# -------------------------------------------------
# WRITE INTO ARRAY BYTE

size_message = 340
f = open("count.txt", "r+")
lines = f.readlines()

for line in lines:
    count = line.split("\n")[0]
    number = int(count)
    arrayCount.append(number)
    if number > max_shark:
        max_shark = number

# Max shark

print("max shark", max_shark)

# Convert array shark to byte
array = bytearray(arrayCount)

# Compress bytes
compressed_data = zlib.compress(array)

length_compressed = len(compressed_data)
print("compressed", length_compressed)

# convert timestamp to bytes
timestamp = int(time.time())
timestampByte = timestamp.to_bytes(4, byteorder='big')

# GPS

lat = "4807.038,N"
long = "01131.324,E"

byte_lat = struct.pack("f", 4807.038)
print(list(byte_lat))
byte_long = struct.pack("f", 1131.324)
byte_long_orientation = bytes("N", 'ascii')
byte_lat_orientation = bytes("E", 'ascii')

array_byte = bytearray()

array_byte += bytearray(byte_long)
array_byte += bytearray(byte_long_orientation)
array_byte += bytearray(byte_lat)
array_byte += bytearray(byte_lat_orientation)

print(list(array_byte))

lat = struct.unpack('f', array_byte[:4])[0]
long = struct.unpack('f', array_byte[5:9])[0]
lat_orientation = array_byte[4:5].decode("ascii")
long_orientation = array_byte[9:10].decode("ascii")

print(lat)
print(long)
print(lat_orientation)
print(long_orientation)


if(length_compressed <= 326):
    number_of_message = 1

else:
    number_of_message = ceil(int(length_compressed - 326)/336 + 1)

# print(number_of_message)

count_compress_data = 0
d = {}
for i in range(number_of_message):

    d["{0}".format(i)] = bytearray()
    d["{0}".format(i)] += bytearray(timestampByte)
    if(i == 0):
        #d["{0}".format(0)] += bytes([max_shark])
        d["{0}".format(0)] += byte_lat
        d["{0}".format(0)] += byte_lat_orientation
        d["{0}".format(0)] += byte_long
        d["{0}".format(0)] += byte_long_orientation
    # print(len(d["{0}".format(i)]))
    while len(d["{0}".format(i)]) < 340 and count_compress_data < length_compressed:

        d["{0}".format(
            i)] += bytearray(bytes([compressed_data[count_compress_data]]))  # add byte by byte to the array till the array reach size 340
        count_compress_data += 1
    # print(len(d["{0}".format(i)]))


sum_compressed_data = sum(d["0"])

sum_bytes = sum_compressed_data.to_bytes(2, byteorder='big')
d["0"] += bytearray(sum_bytes)
"""
# -------------------------------------------------
# COMMUNICATION SERIE
number_gps = 0
size = 340
ser = serial.Serial(
    port='COM4',
    baudrate=19200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=10
)

# 1
# Indicate size of next buffer
print("INDICATE SIZE BUFFER")

buffer = "AT+SBDWB="+str(size)+"\r"
ser.write(buffer.encode())

# 2
# Put data in the buffer
print("--------------------")
print("PUT DATA IN THE BUFFER")

time.sleep(2)
ser.write(d["0"])
time.sleep(5)


# 3
# check number of satellite
while number_gps < 3:
    print("--------------------")
    print("CHECK SATELLITE ")
    command = "AT+CSQ" + "\r"
    ser.write(command.encode())

    # Receive

    while 1:
        rx = ser.readline()
        rx = rx.decode()
        if rx != '':
            print(rx)
            if(rx.startswith("+CSQ:")):
                number_gps = int(rx[5:6])
        else:
            break

    print("Number of GPS available =", number_gps)

    if (number_gps > 1):
        command = "AT+SBDIX"+"\r"
        ser.write(command.encode())

        while 1:
            rx = ser.readline()
            rx = rx.decode()
            if rx != '':
                print(rx)
            else:
                break
"""
