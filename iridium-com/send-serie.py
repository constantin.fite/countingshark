import serial
import time
length = 2
tx = "AT\r"
array = [1, 2, 0, 3, 13]
message = bytes(array)

ser = serial.Serial(
    port='COM4',
    baudrate=19200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=2
)

buffer = "AT+SBDWB="+str(length)+"\r"
#ser.write(tx.encode())
ser.write(buffer.encode())
time.sleep(2)
ser.write(message)

while 1:
    rx = ser.readline()
    rx = rx.decode()
    if rx != '':
        print(rx)
    else:
        break
