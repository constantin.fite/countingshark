#!/bin/bash

DATE=$( date '+%F_%H_%M_%S' )

echo Date : $DATE

##PARAMETERS WHICH CAN BE CHANGED

#Video sampling time in second
TIME=30
#Threshold confidence score 
CONF=0.7
#RESOLUTION
RESOLUTION_W=1280
RESOLUTION_H=720
#FPS
FPS=5

#Delete file video

#Directory to save



# Detection des cameras
/home/xavier/Documents/countingshark/check_camera_connection.sh \

#gnome-terminal -- /bin/sh -c \

# Lecture des coordonnees GPS et ecriture dans le fichier $DATE
/home/xavier/Documents/countingshark/GPS/get_gps.sh $DATE \

# Execution de l'algorithme : Enregistrement puis Detection
/home/xavier/Documents/countingshark/record-rtsp/mosaic.sh $DATE $TIME $RESOLUTION_W $RESOLUTION_H $FPS \
&& /home/xavier/Documents/countingshark/run_detection.sh $DATE $CONF
