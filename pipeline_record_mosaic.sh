#!/bin/bash

DATE=$( date '+%F_%H_%M_%S' )

echo Date : $DATE

#Video sampling time in second
TIME=30
#RESOLUTION
RESOLUTION_W=1280
RESOLUTION_H=720
#FPS
FPS=5

gnome-terminal -- /bin/sh -c \
" /home/xavier/Documents/countingshark/record-rtsp/mosaic.sh '$DATE' $TIME $RESOLUTION_W $RESOLUTION_H $FPS" 
