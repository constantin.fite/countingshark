#!/usr/bin/env python

import json, os, subprocess, datetime

with open('config-ffmpeg.json') as config_file:    
    config = json.load(config_file)

options = config['options']
cameras = config['cameras']

ffmpeg = str(options['ffmpeg'])
basedir = str(options['basedir'])
interval = str(options['interval'])
runtime = str(options['runtime'])
cwd = os.getcwd()

today="\""+str(datetime.datetime.today().year)+"-"+str(datetime.datetime.today().month)+"-" \
+str(datetime.datetime.today().day) +" "+str(datetime.datetime.now().hour)+":" \
+str(datetime.datetime.now().minute)+":"+str(datetime.datetime.now().second)+"\""

print("config:\n")
print("ffmpeg: "+ str(ffmpeg))
print("base directory: "+ str(basedir))
print("file interval: "+ str(interval))
print("total runtime: "+ str(runtime) + "\n")
print("cameras:\n")

for camera in cameras:
  print(str(camera['name']) + ' ' + str(camera['url']))
  if not os.path.exists(basedir+camera['name']):
    os.makedirs(basedir+camera['name'])

  os.chdir(basedir+camera['name'])
  url=camera["url"]
  width=str(camera["width"])
  height=str(camera["height"])
  args=" -y -t 30 -rtsp_transport tcp -thread_queue_size 1024  -i "+url+ " /home/jetson-nano/Documents/shark/countingshark/record-rtsp/"+camera['name']+".mp4"
  print("----------------------")
  subprocess.Popen([ffmpeg+args],shell=True)


#ffmpeg -fflags nobuffer -flags low_delay -strict experimental -y  -t 10 \
#-rtsp_transport tcp -thread_queue_size 1024 \
#-i rtsp://192.168.1.19:554/1/h264major \
#-map 0 /home/jetson-nano/Vidos/19.mp4 \
#-t 10 \
#-rtsp_transport tcp -thread_queue_size 1024 \
#-i rtsp://192.168.1.20:554/1/h264major \
#-map 1  /home/jetson-nano/Vidos/20.mp4

#-fflags nobuffer -flags low_delay -strict experimental
#-rtsp_transport tcp -thread_queue_size 1024
