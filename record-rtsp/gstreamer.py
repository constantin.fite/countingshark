
#!/usr/bin/env python

import json, os, subprocess, datetime

with open('config.json') as config_file:    
    config = json.load(config_file)

options = config['options']
cameras = config['cameras']

gst = str(options['gst'])
basedir = str(options['basedir'])
interval = str(options['interval'])
runtime = str(options['runtime'])
cwd = os.getcwd()


for camera in cameras:
  print(str(camera['name']) + ' ' + str(camera['url']))
  if not os.path.exists(basedir+camera['name']):
    os.makedirs(basedir+camera['name'])

  os.chdir(basedir+camera['name'])
  url=camera["url"]

   
  args=" rtspsrc location="+url+" -e ! rtph264depay ! h264parse ! matroskamux ! filesink location="+str(camera['name'])+".mp4"
  print("----------------------")
  subprocess.Popen([gst+args],shell=True)

