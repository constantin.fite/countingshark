#!/bin/bash

dir="/home/xavier/Documents/countingshark/record-rtsp/records/mosaic/"

mkdir -p $dir

:'
gst-launch-1.0 -e nvcompositor name=mix background-w=2560 background-h=1440 \
    sink_0::xpos=0 sink_0::ypos=0 sink_0::width=1280 sink_0::height=720  \
    sink_1::xpos=1280 sink_1::ypos=0 sink_1::width=1280 sink_1::height=720 \
    sink_2::xpos=0 sink_2::ypos=720 sink_2::width=1280 sink_2::height=720 \
    sink_3::xpos=1280 sink_3::ypos=720 sink_3::width=1280 sink_3::height=720 \
    ! "video/x-raw(memory:NVMM),format=RGBA" ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12" ! nvv4l2h264enc insert-vui=1 profile=2 ! h264parse ! matroskamux ! filesink location=$dir$1.mkv sync=false  \
    rtspsrc location=rtsp://192.168.1.21:554/1/h264major  ! application/x-rtp,media=video,encoding-name=H264 ! rtph264depay ! h264parse ! nvv4l2decoder ! "video/x-raw(memory:NVMM),format=NV12" ! queue ! mix.sink_0      \
    rtspsrc location=rtsp://192.168.1.20:554/1/h264major  ! application/x-rtp,media=video,encoding-name=H264 ! rtph264depay ! h264parse ! nvv4l2decoder ! "video/x-raw(memory:NVMM),format=NV12" ! queue ! mix.sink_1      \
    rtspsrc location=rtsp://192.168.1.19:554/1/h264major  ! application/x-rtp,media=video,encoding-name=H264 ! rtph264depay ! h264parse ! nvv4l2decoder ! "video/x-raw(memory:NVMM),format=NV12" ! queue ! mix.sink_2      \
    rtspsrc location=rtsp://192.168.1.18:554/1/h264major  ! application/x-rtp,media=video,encoding-name=H264 ! rtph264depay ! h264parse ! nvv4l2decoder ! "video/x-raw(memory:NVMM),format=NV12" ! queue ! mix.sink_3    \
& '

gst-launch-1.0 nvcompositor name=mix \
    sink_0::xpos=0 sink_0::ypos=0 sink_0::width=$3 sink_0::height=$4  \
    sink_1::xpos=$3 sink_1::ypos=0 sink_1::width=$3 sink_1::height=$4 \
    sink_2::xpos=0 sink_2::ypos=$4 sink_2::width=$3 sink_2::height=$4 \
    sink_3::xpos=$3 sink_3::ypos=$4 sink_3::width=$3 sink_3::height=$4 \
    ! "video/x-raw(memory:NVMM),format=RGBA,width=$(($3*2)),height=$(($4*2))" ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12" ! nvv4l2h264enc insert-vui=1 profile=2 \
    ! h264parse ! video/x-h264,width=$(($3*2)),height=$(($4*2)),framerate=$5/1 ! matroskamux ! filesink location=$dir$1.mp4 sync=false  \
    rtspsrc location=rtsp://192.168.2.21:554/1/h264major ! application/x-rtp,media=video,encoding-name=H264 ! rtph264depay ! h264parse ! nvv4l2decoder ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12,width=$3,height=$4,pixel-aspect-ratio=1/1" ! queue ! mix.sink_0      \
    rtspsrc location=rtsp://192.168.2.20:554/1/h264major ! application/x-rtp,media=video,encoding-name=H264 ! rtph264depay ! h264parse ! nvv4l2decoder ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12,width=$3,height=$4,pixel-aspect-ratio=1/1" ! queue ! mix.sink_1      \
    rtspsrc location=rtsp://192.168.2.19:554/1/h264major ! application/x-rtp,media=video,encoding-name=H264 ! rtph264depay ! h264parse ! nvv4l2decoder ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12,width=$3,height=$4,pixel-aspect-ratio=1/1" ! queue ! mix.sink_2      \
    rtspsrc location=rtsp://192.168.2.18:554/1/h264major ! application/x-rtp,media=video,encoding-name=H264 ! rtph264depay ! h264parse ! nvv4l2decoder ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12,width=$3,height=$4,pixel-aspect-ratio=1/1" ! queue ! mix.sink_3      \
&


PID=$!

sleep $2

kill -INT $PID

