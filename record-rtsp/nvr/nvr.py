#!/usr/bin/env python

import json, os, subprocess, datetime

with open('config.json') as config_file:    
    config = json.load(config_file)

options = config['options']
cameras = config['cameras']

openrtsp = str(options['openrtsp'])
basedir = str(options['basedir'])
interval = str(options['interval'])
runtime = str(options['runtime'])
cwd = os.getcwd()

today="\""+str(datetime.datetime.today().year)+"-"+str(datetime.datetime.today().month)+"-" \
+str(datetime.datetime.today().day) +""+str(datetime.datetime.now().hour)+":" \
+str(datetime.datetime.now().minute)+":"+str(datetime.datetime.now().second)+"\""

print("config:\n")
print("openRTSP: "+ str(openrtsp))
print("base directory: "+ str(basedir))
print("file interval: "+ str(interval))
print("total runtime: "+ str(runtime) + "\n")
print("cameras:\n")

for camera in cameras:
  print(str(camera['name']) + ' ' + str(camera['url']))
  if not os.path.exists(basedir+camera['name']):
    os.makedirs(basedir+camera['name'])

  os.chdir(basedir+camera['name'])
  url=camera["url"]
  width=str(camera["width"])
  height=str(camera["height"])
  args="  -q -b 10000000 -B 10000000 -f 5 -V -w "+width+" -h "+height+" -d "+runtime+" -P "+interval+" "
  print("----------------------")
  print(openrtsp+args+url)
  subprocess.Popen([openrtsp+args+url],shell=True)

os.chdir(cwd)


