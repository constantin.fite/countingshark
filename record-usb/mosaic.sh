#!/bin/bash

: '
gst-launch-1.0 \
v4l2src device=/dev/video0 ! "image/jpeg,width=1280,height=720, framerate=5/1" ! nvjpegdec ! video/x-raw ! nvvidconv ! \
video/x-raw(memory:NVMM),format=RGBA ! comp.sink_0  \
v4l2src device=/dev/video1 ! "image/jpeg,width=1280,height=720, framerate=5/1" ! nvjpegdec ! video/x-raw ! nvvidconv ! \
video/x-raw(memory:NVMM),format=RGBA ! comp.sink_1 \
nvcompositor name=comp sink_0::xpos=0 sink_0::ypos=0 sink_1::xpos=1280 sink_1::ypos=0  \
! nvvidconv ! nvv4l2h264enc  ! h264parse ! qtmux ! filesink location=test.mp4 -e


gst-launch-1.0 \
v4l2src io-mode=2 device=/dev/video0 ! "image/jpeg,width=1920,height=1080, framerate=30/1" ! nvjpegdec ! video/x-raw ! nvvidconv ! "video/x-raw(memory:NVMM),format=RGBA" ! comp.sink_0  \
v4l2src io-mode=2 device=/dev/video1 ! "image/jpeg,width=640,height=360, framerate=30/1" ! nvjpegdec ! video/x-raw ! nvvidconv ! "video/x-raw(memory:NVMM),format=RGBA" ! comp.sink_1 \
nvcompositor  name=comp sink_0::xpos=0 sink_0::ypos=0 sink_1::xpos=1280 sink_1::ypos=0 ! \
nvvidconv ! nvv4l2h264enc maxperf-enable=1 bitrate=4000000 profile=4 ! h264parse ! qtmux ! filesink location=test_MJPG_H264enc_video1_rgba.mp4 -e

#working
gst-launch-1.0 -ev \
v4l2src device=/dev/video0 ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12,width=1280,height=720,pixel-aspect-ratio=1/1" ! queue ! mix.sink_0 \
v4l2src device=/dev/video1 ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12,width=1280,height=720,pixel-aspect-ratio=1/1" ! queue ! mix.sink_1 \
nvcompositor name=mix \
sink_0::xpos=0 sink_0::ypos=0 sink_0::width=1280 sink_0::height=720 \
sink_1::xpos=1280 sink_1::ypos=0 sink_1::width=1280 sink_1::height=720 \
! "video/x-raw(memory:NVMM),format=RGBA" ! nvvidconv ! nvv4l2h264enc insert-vui=1 ! h264parse ! matroskamux ! filesink location=test.mkv sync=false 
'

gst-launch-1.0 -ev \
v4l2src device=/dev/video0 ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12,width=800,height=600,pixel-aspect-ratio=1/1,framerate=5/1" ! queue ! mix.sink_0 \
v4l2src device=/dev/video1 ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12,width=800,height=600,pixel-aspect-ratio=1/1,framerate=5/1" ! queue ! mix.sink_1 \
v4l2src device=/dev/video2 ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12,width=800,height=600,pixel-aspect-ratio=1/1,framerate=5/1" ! queue ! mix.sink_2 \
v4l2src device=/dev/video3 ! nvvidconv ! "video/x-raw(memory:NVMM),format=NV12,width=800,height=600,pixel-aspect-ratio=1/1,framerate=5/1" ! queue ! mix.sink_3 \
nvcompositor name=mix \
sink_0::xpos=0 sink_0::ypos=0 sink_0::width=800 sink_0::height=600 \
sink_1::xpos=800 sink_1::ypos=0 sink_1::width=800 sink_1::height=600 \
sink_2::xpos=0 sink_0::ypos=600 sink_2::width=800 sink_2::height=600 \
sink_3::xpos=800 sink_1::ypos=600 sink_3::width=800 sink_3::height=600 \
! "video/x-raw(memory:NVMM),format=RGBA" ! nvvidconv ! nvv4l2h264enc insert-vui=1 ! h264parse ! matroskamux ! filesink location=test_hub.mkv sync=false 



