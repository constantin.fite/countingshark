#!/bin/bash

gst-launch-1.0 -e \
v4l2src device=/dev/video0 ! videoconvert ! videoscale ! video/x-raw, width=1280, height=720, framerate=5/1  \
! queue ! nvvidconv ! queue ! "video/x-raw(memory:NVMM),format=RGBA" ! queue ! comp.sink_0 \
v4l2src device=/dev/video1 ! videoconvert ! videoscale ! video/x-raw, width=1280, height=720, framerate=5/1  \
! queue ! nvvidconv ! queue ! "video/x-raw(memory:NVMM),format=RGBA" ! queue ! comp.sink_1 \
 nvcompositor name=comp \
 sink_0::xpos=0 sink_0::ypos=0 sink_0::width=1280 sink_0::height=720 \
 sink_1::xpos=1280 sink_1::ypos=0 sink_1::width=1280 sink_1::height=720 \
 ! nvvidconv ! nvv4l2h264enc maxperf-enable=1 bitrate=4000000 profile=4  ! h264parse ! qtmux ! filesink location="test1.mp4"


 # sink_2::xpos=0 sink_2::ypos=720 sink_2::width=1280 sink_2::height=720 \
 # sink_3::xpos=1280 sink_3::ypos=720 sink_3::width=1280 sink_3::height=720 \

#v4l2src device=/dev/video2 ! videoconvert ! videoscale ! video/x-raw, width=1280, height=720, framerate=25/1  \
#! queue ! nvvidconv ! queue ! "video/x-raw(memory:NVMM),format=RGBA" ! queue ! comp.sink_2 \
#v4l2src device=/dev/video3 ! videoconvert ! videoscale ! video/x-raw, width=1280, height=720, framerate=25/1  \
#! queue ! nvvidconv ! queue ! "video/x-raw(memory:NVMM),format=RGBA" ! queue ! comp.sink_3 \
