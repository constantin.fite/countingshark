#!/bin/bash


xhost +local:

docker run --rm --runtime nvidia -e DISPLAY=$DISPLAY --network host -v \
 /home/xavier/Documents/countingshark:/countingshark -it yolov5-docker /bin/sh -c "cd ./yolov5-3.1 && python3 detect.py \
 --source ../record-rtsp/records/mosaic/$1.mp4 --weights ../best-shark-yolov5s.pt --conf $2 --name $1 --img 1280 --classes 1"

