import natsort
import glob
import os
import cv2

path = '/home/jetson-nano/Documents/shark/countingshark/video/soyeux2/extracted_frame_5FPS/'
img_array = []
for count in range(len(os.listdir(path))):

    filename = path+'frame'+str(count+1)+'.jpg'
    if os.path.isfile(filename):
        img = cv2.imread(filename)
        height, width, layers = img.shape
        size = (width, height)
        img_array.append(img)
        print(count)

out = cv2.VideoWriter('project.avi', cv2.VideoWriter_fourcc(*'DIVX'), 5, size)
for i in range(len(img_array)):
    out.write(img_array[i])
out.release()


filenames = glob.glob("images/*.jpg")
filenames.sort()
image_folder = r'C:\Users\const\Documents\PFE Requin\Code\countingshark\video\soyeux2\extracted_frame_5FPS\\'
video_name = 'video.avi'
files = os.listdir(image_folder)
sorted_files = natsort.natsorted(files, reverse=False)


images = [img for img in sorted_files if img.endswith(".jpg")]
frame = cv2.imread(os.path.join(image_folder, images[0]))
height, width, layers = frame.shape
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
video = cv2.VideoWriter(video_name, fourcc, 5, (width, height))

for image in images:
    video.write(cv2.imread(os.path.join(image_folder, image)))

cv2.destroyAllWindows()
video.release()
